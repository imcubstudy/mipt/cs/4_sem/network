#ifndef common_h
#define common_h

#include <stdio.h>
#include <stdlib.h>

#define _str(x) #x
#define str(x) _str(x)
#define TEST(cond, msg...)                                          \
    do {                                                            \
        if(!(cond)) {                                               \
            fprintf(stderr, msg);                                   \
            fprintf(stderr, "\n");                                  \
            fflush(stderr);                                         \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

#define _cmp(op, x, y)                                              \
    ({                                                              \
        typeof(x) __x = (x);                                        \
        typeof(y) __y = (y);                                        \
        __x op __y ? __x : __y;                                     \
    })
#define min(x, y) _cmp(<, x, y)
#define max(x, y) _cmp(>, x, y)

// integral related
typedef struct {
    double      lb;
    double      hb;
    double      res;
} targs_t;

// log related
#define PRINT_ENABLE    3

#define _PRINTF(lvl, c, args...)                                \
    do {                                                        \
        if(PRINT_ENABLE >= lvl) {                               \
            fprintf(stderr, "\e[" str(c) "m" args);             \
            fprintf(stderr, " at " str(__LINE__)                \
                            " from " __FILE__ "\e[0m\n");       \
            fflush(stderr);                                     \
        }                                                       \
    } while(0)

#define EPRINTF(args...)    _PRINTF(0, 91, args)    // printf error
#define ELPRINTF(args...)   _PRINTF(1, 91, args)    // printf lib error
#define WPRINTF(args...)    _PRINTF(2, 93, args)    // printf warning
#define LPRINTF(args...)    _PRINTF(3, 0,  args)    // printf log

#endif /* common_h */
