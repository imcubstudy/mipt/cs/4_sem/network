#include "common.h"
#include "imcubnet.h"

#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/syscall.h>
#define gettid() syscall(SYS_gettid)
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

socket_t getudpbroadcastsk(in_port_t port)
{
    socket_t sk = -1;
    if(0 > (sk = socket(AF_INET, SOCK_DGRAM, 0))) {
        ELPRINTF("socket failed");
        goto ehandle_1;
    }

    int bc_val = 1;
    if(0 > setsockopt(sk, SOL_SOCKET, SO_BROADCAST, &bc_val, sizeof(bc_val))) {
        ELPRINTF("setsockopt SO_BROADCAST failed");
        goto ehandle_2;
    }

    int ra_val = 1;
    if(0 > setsockopt(sk, SOL_SOCKET, SO_REUSEADDR, &ra_val, sizeof(ra_val))) {
        ELPRINTF("setsockopt SO_REUSEADDR failed");
        goto ehandle_2;
    }

    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port   = port,
        .sin_addr   = INADDR_BROADCAST
    };
    if(0 > bind(sk, (void*)&addr, sizeof(addr))) {
        ELPRINTF("bind failed");
        goto ehandle_2;
    }

    return sk;

ehandle_2:
    close(sk);
ehandle_1:
    return -1;
}

int udpwaitmsgfrom(socket_t sk, struct sockaddr_in *src, void *msg, int msgsz)
{
    do{
        socklen_t addrlen = sizeof(*src);

        char tmp[msgsz];
        if(0 > recvfrom(sk, tmp, msgsz, 0, (void*)src, &addrlen)) {
            ELPRINTF("recvfrom failed");
            return -1;
        }

        if(memcmp(msg, tmp, msgsz) == 0) {
            LPRINTF("msg received");
            return 0;
        } else {
            WPRINTF("wrong msg received");
        }
    } while(1);
}

int udpbroadcastmsg(in_port_t port, void *msg, int msgsz)
{
    socket_t sk = -1;
    if(0 > (sk = getudpbroadcastsk(port))) {
        ELPRINTF("getudpbroadcastsk failed");
        goto ehandle_1;
    }
    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port   = port,
        .sin_addr   = INADDR_BROADCAST
    };
    if(0 > sendto(sk, msg, msgsz, 0, (void*)&addr, sizeof(addr))) {
        ELPRINTF("sendto failed");
        goto ehandle_2;
    }

    close(sk);
    return 0;

ehandle_2:
    close(sk);
ehandle_1:
    return -1;
}

socket_t gettcplistensk(in_port_t port, int maxlisten)
{
    socket_t sk = -1;
    if(0 > (sk = socket(AF_INET, SOCK_STREAM, 0))) {
        ELPRINTF("socket failed");
        goto ehandle_1;
    }

    int ra_val = 1;
    if(0 > setsockopt(sk, SOL_SOCKET, SO_REUSEADDR, &ra_val, sizeof(ra_val))) {
        ELPRINTF("setsockopt SO_REUSEADDR failed");
        goto ehandle_2;
    }

    struct sockaddr_in addr = {
        .sin_family = AF_INET,
        .sin_port   = port,
        .sin_addr   = INADDR_ANY
    };
    if(0 > bind(sk, (void*)&addr, sizeof(addr))) {
        ELPRINTF("bind failed");
        goto ehandle_2;
    }

    if(0 > listen(sk, maxlisten)) {
        ELPRINTF("listen failed");
        goto ehandle_2;
    }

    return sk;

ehandle_2:
    close(sk);
ehandle_1:
    return -1;
}

int tcpacceptconnections(socket_t sk, socket_t *sks, int maxn,
                         struct timeval *timeout)
{
    fd_set set;
    FD_ZERO(&set);
    FD_SET(sk, &set);

    int nsk = 0;
    for(nsk = 0; nsk < maxn; ++nsk) {
        int ret = -1;
        if(0 > (ret = select(sk + 1, &set, NULL, NULL, timeout))) {
            ELPRINTF("select failed");
            goto ehandle;
        }
        if(ret == 0) {
            WPRINTF("select timed out");
            break;
        }

        struct sockaddr_in addr;
        socklen_t addrlen = sizeof(addr);
        if(0 > (ret = accept(sk, (void*)&addr, &addrlen))) {
            ELPRINTF("accept failed");
            goto ehandle;
        }

        sks[nsk] = ret;
        LPRINTF("connection %d with %s, tm left %d",
                nsk + 1, inet_ntoa(addr.sin_addr), timeout->tv_usec);
    }

    for(int i = 0; i < nsk; ++i) {
        if(0 > tcpsetown(sks[i])) {
            ELPRINTF("tcpsetown failed");
            goto ehandle;
        }
        if(0 > tcpkaenable(sks[i])) {
            ELPRINTF("tcpkaenable failed");
            goto ehandle;
        }
    }

    return nsk;

ehandle:
    while(nsk--)
        close(sks[nsk]);

    return -1;
}

socket_t tcpconnect(struct sockaddr_in *addr, struct timeval *timeout)
{
    socket_t sk = -1;
    if(0 > (sk = socket(AF_INET, SOCK_STREAM, 0))) {
        ELPRINTF("socket failed");
        goto ehandle_1;
    }
    if(0 > fcntl(sk, F_SETFL, O_NONBLOCK)) {
        ELPRINTF("fcntl failed");
        goto ehandle_2;
    }
    if(0 > tcpkaenable(sk)) {
        ELPRINTF("tcpkaenable failed");
        goto ehandle_2;
    }

    fd_set set;
    FD_ZERO(&set);
    FD_SET(sk, &set);

    if(connect(sk, (void*)addr, sizeof(*addr)) < 0 && errno == EINPROGRESS) {
        errno = 0;
        if(0 > select(sk + 1, NULL, &set, NULL, timeout)) {
            if(errno != 0) {
                ELPRINTF("select failed");
            } else {
                ELPRINTF("select timed out");
            }
            goto ehandle_2;
        }

        int errval = 0;
        socklen_t len = sizeof(errval);
        if(0 > getsockopt(sk, SOL_SOCKET, SO_ERROR, &errval, &len)) {
            ELPRINTF("getsockopt failed");
            goto ehandle_2;
        }

        if(errval != 0) {
            ELPRINTF("connection error : %d : %s", errval, strerror(errval));
            goto ehandle_2;
        }
    }

    if(0 > fcntl(sk, F_SETFL, 0)) {
        ELPRINTF("fcntl failed");
        goto ehandle_2;
    }
    if(0 > tcpsetown(sk)) {
        ELPRINTF("tcpsetown failed");
        goto ehandle_2;
    }

    return sk;

ehandle_2:
    close(sk);
ehandle_1:
    return -1;
}

int tcpkaenable(socket_t tcp_sk)
{
    int val_ka = KA_VAL;
    if(0 > setsockopt(tcp_sk, SOL_SOCKET, SO_KEEPALIVE, &val_ka, sizeof(val_ka))) {
        ELPRINTF("setsockopt SO_KEEPALIVE failed");
        return -1;
    }

    int val_idle = KA_IDLE;
    if(0 > setsockopt(tcp_sk, IPPROTO_TCP, TCP_KEEPIDLE, &val_idle, sizeof(val_idle))){
        ELPRINTF("setsockopt TCP_KEEPIDLE failed");
        return -1;
    }

    int val_intvl = KA_INTVL;
    if(0 > setsockopt(tcp_sk, IPPROTO_TCP, TCP_KEEPINTVL, &val_intvl, sizeof(val_intvl))){
        ELPRINTF("setsockopt TCP_KEEPINTVL failed");
        return -1;
    }

    int val_cnt = KA_CNT;
    if(0 > setsockopt(tcp_sk, IPPROTO_TCP, TCP_KEEPCNT, &val_cnt, sizeof(val_cnt))){
        ELPRINTF("setsockopt TCP_KEEPCNT failed");
        return -1;
    }

    return 0;
}

ssize_t tcpread(socket_t sk, void *buf, size_t sz)
{
    ssize_t ret = -1;
    if(0 >= (ret = read(sk, buf, sz))) {
        if(ret == 0) {
            ELPRINTF("Connection lost");
        } else {
            ELPRINTF("read failed");
        }
        return -1;
    }

    if(ret != sz) {
        ELPRINTF("Nonfull read");
        return -1;
    }

    return ret;
}

ssize_t tcpwrite(socket_t sk, void *buf, size_t sz)
{
    ssize_t ret = -1;
    if(0 >= (ret = write(sk, buf, sz))) {
        if(ret == 0) {
            ELPRINTF("Connection lost");
        } else {
            ELPRINTF("write failed");
        }
        return -1;
    }

    if(ret != sz) {
        ELPRINTF("Nonfull write");
        return -1;
    }

    return ret;
}

jmp_buf sig_exception;
void sigio_action(int sig)
{
    ELPRINTF("Caught sig");
    longjmp(sig_exception, sig);
}

void initsighandlers()
{
    struct sigaction act_ign = {
        .sa_handler = SIG_IGN
    };
    if(0 > sigaction(SIGPIPE, &act_ign, NULL))
        goto ehandle;
    if(0 > sigaction(SIGURG, &act_ign, NULL))
        goto ehandle;

    struct sigaction act_io = {
        .sa_handler   = sigio_action,
    };
    if(0 > sigaction(SIGIO, &act_io, NULL))
        goto ehandle;

    LPRINTF("Succesfully initialised signal handlers");
    return;

ehandle:
    ELPRINTF("sigaction failed");
    exit(EXIT_FAILURE);
}

int tcpsetown(socket_t sk)
{
    struct f_owner_ex self = {
        .type = F_OWNER_PID,
        .pid  = gettid()
    };
    if(0 > fcntl(sk, F_SETOWN_EX, &self)) {
        ELPRINTF("fcntl setown failed");
        return -1;
    }
    return 0;
}
