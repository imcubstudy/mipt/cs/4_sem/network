#ifndef integrmth_h
#define integrmth_h

#include "common.h"
#include <pthread.h>

#define dX      4e-8
#define FUNC(x) (0.25 * (x)*(x) - (x))

#ifndef CPUINFO
    #error "CPUINFO not defined"
#endif

#define MAXTHPCPU       128
#define MAXCORES        64
#define MAXCPUPCORE     2
#define HTK             1.6

struct {
    int ncores;
    int ncpu;
    int ntpc;
    int cpu[MAXCORES][MAXCPUPCORE];
} cpuinfo;

typedef struct {
    targs_t         args;
    pthread_t       tid;
} tinfo_t;

typedef struct {
    struct {
        tinfo_t th[MAXTHPCPU];
        int     n;
        int     comp;
    } cpu[MAXTHPCPU];
} core_threadset_t;

typedef struct {
    int     nthreads;
    int     nhalf;
    int     nfull;
    double  perf;
    int     *load;
} coreload_t;

typedef struct {
    coreload_t          cl;
    double              perf;
    core_threadset_t    *cts;
} threadset_t;
extern threadset_t threadset;

int  compute(targs_t *task);
void resolvecores(int amount);
void distribute(targs_t *task);
void *routine(void *arg);

void parseCPUINFO(void)         __attribute__((constructor(101)));
void initthreadset(void)        __attribute__((constructor(102)));
void destructthreadset(void)    __attribute__((destructor()));

#endif /* integrmth_h */
