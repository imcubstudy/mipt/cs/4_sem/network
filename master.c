#include "common.h"
#include "imcubnet.h"

#include <sys/select.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#define LOWB    -1000e0
#define HIGHB   1000e0

typedef struct {
    int     sk;
    double  perf;
    targs_t task;
} worker_t;

int getworkers(worker_t *workers, int maxworkers,
               in_port_t udp_port, in_port_t tcp_port);
int releaseworkers(worker_t *workers, int amount);

int     distribute  (worker_t *workers, int amount);
int     sendtasks   (worker_t *workers, int amount);
int     collecttasks(worker_t *workers, int amount, double *res);
double  compute     (worker_t *workers, int amount);

int main(int argc, char **argv)
{
    TEST(argc == 4, "Wrong amount of arguments:\n"
                    "\tUsage %s [nworkers] [udp_port] [tcp_port]", argv[0]);
    int nworkers = atoi(argv[1]);
    TEST(nworkers <= MAXWORKERS, "That's too mush, maaan!");

    in_port_t udp_port = htons(atoi(argv[2]));
    in_port_t tcp_port = htons(atoi(argv[3]));

    worker_t workers[nworkers];
    memset(workers, 0, sizeof(workers));

    int amount = -1;
    if(0 > (amount = getworkers(workers, nworkers, udp_port, tcp_port))) {
        EPRINTF("getworkers failed");
        goto ehandle_1;
    }

    if(0 > distribute(workers, amount)) {
        EPRINTF("distribute failed");
        goto ehandle_2;
    }

    double res = NAN;
    if(isnan(res = compute(workers, amount))) {
        EPRINTF("compute failed");
        goto ehandle_2;
    }

    if(0 > releaseworkers(workers, amount)) {
        EPRINTF("releaseworkers failed");
        exit(EXIT_FAILURE);
    }

    printf("res = %lf\n", res);

    exit(EXIT_SUCCESS);

ehandle_2:
    releaseworkers(workers, amount);
ehandle_1:
    exit(EXIT_FAILURE);
}

int getworkers(worker_t *workers, int maxworkers,
               in_port_t udp_port, in_port_t tcp_port)
{
    socket_t tcp_sk = -1;
    if(0 > (tcp_sk = gettcplistensk(tcp_port, maxworkers))) {
        EPRINTF("gettcplistensk failed");
        return -1;
    }

    int sig = SIGNATURE;
    if(0 > udpbroadcastmsg(udp_port, &sig, sizeof(sig))) {
        EPRINTF("udpbroadcastmsg failed");
        close(tcp_sk);
        return -1;
    }

    struct timeval tv = {
        .tv_usec    = INET_TIMEOUT
    };
    socket_t connections[maxworkers];
    memset(connections, 0, sizeof(connections));

    int nc = -1;
    if(0 > (nc = tcpacceptconnections(tcp_sk, connections, maxworkers, &tv))) {
        EPRINTF("tcpacceptconnections failed");
        goto ehandle;
    }
    close(tcp_sk);

    for(int i = 0; i < nc; ++i) {
        workers[i].sk = connections[i];
    }

    return nc;

ehandle:
    close(tcp_sk);
    return -1;
}

int releaseworkers(worker_t *workers, int amount)
{
    for(int i = 0; i < amount; ++i) {
        if(0 > close(workers[i].sk)) {
            EPRINTF("close failed");
            return -1;
        }
    }
}

int distribute(worker_t *workers, int amount)
{
    double totalperf = 0;
    for(int i = 0; i < amount; ++i) {
        if(0 > tcpread(workers[i].sk, &workers[i].perf, sizeof(double))) {
            EPRINTF("tcpread failed");
            return -1;
        }
        totalperf += workers[i].perf;
        LPRINTF("recv perf = %lf from %d", workers[i].perf, i + 1);
    }

    double delt = (HIGHB - LOWB) / totalperf;
    double lb = LOWB;
    for(int perf = 0, i = 0; i < amount; i++) {
        workers[i].task.lb = lb + delt * perf;
        workers[i].task.hb = lb + delt * (perf + workers[i].perf);
        lb = workers[i].task.hb;
        perf += workers[i].perf;
    }

    return 0;
}

double compute(worker_t *workers, int amount)
{
    if(0 > sendtasks(workers, amount)) {
        EPRINTF("sendtasks failed");
        return NAN;
    }

    double res = 0;
    if(0 > collecttasks(workers, amount, &res)) {
        EPRINTF("collecttasks failed");
        return NAN;
    }

    return res;
}

int sendtasks(worker_t *workers, int amount)
{
    for(int i = 0; i < amount; ++i) {
        LPRINTF("sending task to %d", i + 1);
        if(0 > tcpwrite(workers[i].sk, &workers[i].task, sizeof(targs_t))) {
            EPRINTF("tcpwrite failed");
            return -1;
        }
    }
    return 0;
}

int collecttasks(worker_t *workers, int amount, double *res)
{
    fd_set set, tmp;
    FD_ZERO(&set);

    socket_t max_sk = -1;
    for(int i = 0; i < amount; ++i) {
        FD_SET(workers[i].sk, &set);
        max_sk = max(workers[i].sk, max_sk);
    }

    double tres = 0;
    while(amount) {
        tmp = set;
        int ready = -1;
        if(0 > (ready = select(max_sk + 1, &tmp, NULL, NULL, NULL))) {
            EPRINTF("select failed");
            return -1;
        }

        for(int i = 0; ready; ++i) {
            if(!FD_ISSET(workers[i].sk, &set))
                continue;

            targs_t wres = {};
            if(0 > tcpread(workers[i].sk, &wres, sizeof(wres))) {
                EPRINTF("tcpread failed");
                return -1;
            }
            tres += wres.res;
            LPRINTF("res from %d accepted", i + 1);

            ready--;
            amount--;
            FD_CLR(workers[i].sk, &set);
        }
    }

    *res = tres;
    return 0;
}
