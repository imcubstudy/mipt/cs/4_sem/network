#include "common.h"
#include "imcubnet.h"
#include "integrmth.h"

#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>

socket_t getmaster(socket_t udp_sk, in_port_t tcp_port);

int main(int argc, char** argv)
{
    TEST(argc == 4, "Wrong amount of arguments:\n"
                    "\tUsage %s [nthreads] [udp_port] [tcp_port]", argv[0]);
    resolvecores(atoi(argv[1]));

    in_port_t udp_port = htons(atoi(argv[2]));
    in_port_t tcp_port = htons(atoi(argv[3]));

    int udp_sk = -1;
    if(0 > (udp_sk = getudpbroadcastsk(udp_port))) {
        EPRINTF("getudpbroadcastsk failed");
        goto ehandle_g;
    }

    do {
        socket_t msk = -1;
        if(0 > (msk = getmaster(udp_sk, tcp_port))) {
            EPRINTF("getmaster failed");
            goto ehandle_1;
        }

        if(0 > tcpwrite(msk, &(threadset.cl.perf), sizeof(threadset.cl.perf))) {
            EPRINTF("tcpwrite failed");
            goto ehandle_2;
        }
        LPRINTF("sent performance = %lf", threadset.cl.perf);

        targs_t task = {};
        if(0 > tcpread(msk, &task, sizeof(task))) {
            EPRINTF("tcpread failed");
            goto ehandle_2;
        }
        LPRINTF("received tsk %lf - %lf", task.lb, task.hb);

        CONNECTION_LOST_PROTECTED(msk, ehandle_2, {
            if(0 > compute(&task)) {
                EPRINTF("compute failed");
                exit(EXIT_FAILURE);
            }});

        if(0 > tcpwrite(msk, &task, sizeof(task))) {
            EPRINTF("tcpwrite failed");
            goto ehandle_2;
        }
        LPRINTF("sent res = %lf", task.res);

        if(0 > close(msk)) {
            EPRINTF("close failed");
            goto ehandle_g;
        }

        printf("Request done\n");
        continue;

    ehandle_2:
        close(msk);
    ehandle_1:
        continue;

    } while(1);

ehandle_g:
    exit(EXIT_FAILURE);
}

socket_t getmaster(socket_t udp_sk, in_port_t tcp_port)
{
    int sig = SIGNATURE;
    struct sockaddr_in master_addr;
    if(0 > udpwaitmsgfrom(udp_sk, &master_addr, &sig, sizeof(sig))) {
        EPRINTF("udpwaitmsgfrom failed");
        return -1;
    }
    printf("request from %s\n", inet_ntoa(master_addr.sin_addr));

    master_addr.sin_port = tcp_port;
    socket_t tcp_sk = -1;
    struct timeval tv = {
        .tv_sec     = 0,
        .tv_usec    = INET_TIMEOUT
    };
    if(0 > (tcp_sk = tcpconnect(&master_addr, &tv))) {
        EPRINTF("tcpconnect failed");
        return -1;
    }
    LPRINTF("connected to master at %s", inet_ntoa(master_addr.sin_addr));

    return tcp_sk;
}
