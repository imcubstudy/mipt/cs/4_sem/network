#ifndef imcubnet_h
#define imcubnet_h

#ifndef __linux__
    #error "This is linux-specific code, macro __linux__ not detected"
#endif

#include <sys/types.h>
#include <fcntl.h>
#include <setjmp.h>
#include <netinet/ip.h>

#define SIGNATURE       0xbeafbeda
#define MAXWORKERS      12
#define INET_TIMEOUT    1000 * 1000
#define KA_VAL          1
#define KA_IDLE         1
#define KA_INTVL        1
#define KA_CNT          1

typedef int socket_t;

socket_t getudpbroadcastsk(in_port_t port);
int udpwaitmsgfrom(socket_t sk, struct sockaddr_in *src, void *msg, int msgsz);
int udpbroadcastmsg(in_port_t port, void *msg, int msgsz);


socket_t gettcplistensk(in_port_t port, int maxlisten);
int tcpacceptconnections(socket_t sk, socket_t *sks, int maxn,
                         struct timeval *timeout);
socket_t tcpconnect(struct sockaddr_in *addr, struct timeval *timeout);
int tcpkaenable(socket_t tcp_sk);

ssize_t tcpread (socket_t sk, void *buf, size_t sz);
ssize_t tcpwrite(socket_t sk, void *buf, size_t sz);

void initsighandlers(void) __attribute__((constructor()));
int tcpsetown(socket_t sk);

extern jmp_buf sig_exception;
#define CONNECTION_LOST_PROTECTED(sk, handler, code...)     \
    do {                                                    \
        if(0 != setjmp(sig_exception)) {                    \
            EPRINTF("Connection lost");                     \
            goto handler;                                   \
        }                                                   \
        if(0 > fcntl(sk, F_SETFL, O_ASYNC)) {               \
            EPRINTF("fcntl O_ASYNC failed");                \
            goto handler;                                   \
        }                                                   \
        code                                                \
        if(0 > fcntl(msk, F_SETFL, 0)) {                    \
            EPRINTF("fcntl O_ASYNC failed");                \
            goto handler;                                   \
        }                                                   \
    } while(0)

#endif /* imcubnet_h */
