CC := gcc
CPUINFO := $(shell lscpu -p=Core,CPU |                                          \
                   tr ',' ' ' |                                                 \
                   egrep "[0-9]+ [0-9]+")

CFLAGS = -pthread -O2 -DCPUINFO="$(CPUINFO)" -D_GNU_SOURCE

-include $(OBJS:.o=.d)

all: master worker

%.o: %.c %.h common.h
	@$(CC) -c $(CFLAGS) $< -o $@
	@echo "$@ compiled"

master: master.o imcubnet.o
	@$(CC) $(CFLAGS) $^ -o $@
	@echo "$@ compiled"

worker: worker.o imcubnet.o integrmth.o
	@$(CC) $(CFLAGS) $^ -o $@
	@echo "$@ compiled"

clean:
	@$(RM) *.o
	@$(RM) master
	@$(RM) worker

