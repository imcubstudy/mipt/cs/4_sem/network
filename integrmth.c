#include "common.h"
#include "integrmth.h"

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sched.h>

threadset_t threadset = {};

void parseCPUINFO()
{
    char info[] = str(CPUINFO);
    int sh = 0, delt = -1;
    int core, cpu;
    while(({sscanf(info + sh, "%d%d%n", &core, &cpu, &delt);
        sh += delt;
        *(info + sh - delt);
    })) {
        cpuinfo.ncores = max(core + 1, cpuinfo.ncores);
        cpuinfo.ncpu++;
    }
    cpuinfo.ntpc = cpuinfo.ncpu / cpuinfo.ncores;

    int init[cpuinfo.ncores];
    memset(init, 0, sizeof(int) * cpuinfo.ncores);

    sh = 0;
    while(({sscanf(info + sh, "%d%d%n", &core, &cpu, &delt);
        sh += delt;
        *(info + sh - delt);
    })) {
        cpuinfo.cpu[core][init[core]] = cpu;
        init[core]++;
    }
}

void initthreadset()
{
    threadset.cl.nthreads  = 0;
    threadset.cl.nhalf     = 0;
    threadset.cl.nfull     = 0;
    threadset.cl.perf      = 0;
    if(NULL == (threadset.cl.load = calloc(cpuinfo.ncores, sizeof(int)))) {
        EPRINTF("memory error");
        exit(EXIT_FAILURE);
    }

    if(NULL == (threadset.cts = calloc(cpuinfo.ncores, sizeof(core_threadset_t)))) {
        EPRINTF("memory error");
        exit(EXIT_FAILURE);
    }
}

void resolvecores(int amount)
{
    TEST(amount <= MAXTHPCPU * cpuinfo.ncpu, "That's too much, maaan!");

    int stop = 0;
    int resolved = 0;
    for(int t = 0; !stop; t = (t + 1) % cpuinfo.ntpc) {
        for(int c = 0; !stop && c < cpuinfo.ncores; c++) {
            threadset.cts[c].cpu[t].n++;
            threadset.cts[c].cpu[t].comp += (resolved < amount) ?
            1 : 0;

            resolved++;
            if(resolved >= amount && resolved >= cpuinfo.ncores)
                stop = 1;
        }
    }

    for(int c = 0; c < cpuinfo.ncores; ++c) {
        int rate = 0;
        for(int t = 0; t < cpuinfo.ntpc; ++t)
            rate += (threadset.cts[c].cpu[t].comp > 0) ? 1 : 0;
        if(rate == 1) {
            threadset.cl.nhalf += rate;
        } else if(rate == 2) {
            threadset.cl.nfull += rate;
        }
        threadset.cl.load[c] = rate;
    }
    if(cpuinfo.ntpc == 1) {
        threadset.cl.nfull = threadset.cl.nhalf;
        threadset.cl.nhalf = 0;
    }

    threadset.cl.nthreads = amount;
    threadset.cl.perf     = (threadset.cl.nhalf * HTK + threadset.cl.nfull) / cpuinfo.ncpu;
}

void distribute(targs_t *task)
{
    double lgth = task->hb - task->lb;

    double LF = lgth / (threadset.cl.nhalf * HTK + threadset.cl.nfull);
    double LH = LF * HTK;
    if(cpuinfo.ntpc == 1) {
        LH = LF;
    }
    
    double lb = task->lb;
    for(int c = 0; c < cpuinfo.ncores; ++c) {
        double Lcore = (threadset.cl.load[c] == 1) ? LH : 2 * LF;

        double Lcpu = Lcore / threadset.cl.load[c];
        for(int t = 0; t < cpuinfo.ntpc; ++t) {

            double delt = Lcpu / threadset.cts[c].cpu[t].comp;
            for(int i = 0; i < threadset.cts[c].cpu[t].comp; ++i) {
                targs_t *args = &threadset.cts[c].cpu[t].th[i].args;

                args->lb = lb;
                args->hb = lb + delt;
                lb += delt;
            }
            for(int i = threadset.cts[c].cpu[t].comp; i < threadset.cts[c].cpu[t].n; ++i) {
                targs_t *args = &threadset.cts[c].cpu[t].th[i].args;

                args->lb = 0;
                args->hb = Lcore;
            }
        }
    }
}

int compute(targs_t *task)
{
    distribute(task);

    cpu_set_t       set;
    pthread_attr_t  pta;
    if(0 != pthread_attr_init(&pta)) {
        EPRINTF("pthread_attr_init failed");
        return -1;
    }
    for(int t = 0; t < cpuinfo.ntpc; ++t) {
        for(int c = 0; c < cpuinfo.ncores; ++c) {
            CPU_ZERO(&set);
            CPU_SET(cpuinfo.cpu[c][t], &set);
            if(0 != pthread_attr_setaffinity_np(&pta, sizeof(set), &set)) {
                EPRINTF("pthread_attr_setaffinity_np failed");
                return -1;
            }
            for(int i = 0; i < threadset.cts[c].cpu[t].n; ++i) {
                tinfo_t *th = &threadset.cts[c].cpu[t].th[i];
                if(0 != pthread_create(&(th->tid), &pta, routine, &(th->args))) {
                    EPRINTF("pthread_create failed");
                    return -1;
                }
            }
        }
    }
    if(0 != pthread_attr_destroy(&pta)) {
        EPRINTF("pthread_create failed");
        return -1;
    }
    double res = 0;
    for(int t = 0; t < cpuinfo.ntpc; ++t) {
        for(int c = 0; c < cpuinfo.ncores; ++c) {
            for(int i = 0; i < threadset.cts[c].cpu[t].comp; ++i) {
                if(0 != pthread_join(threadset.cts[c].cpu[t].th[i].tid, NULL)) {
                    EPRINTF("pthread_join failed");
                    return -1;
                }
                res += threadset.cts[c].cpu[t].th[i].args.res;
            }
        }
    }

    task->res = res;

    return 0;
}

void *routine(void *arg)
{
    targs_t *targs = (targs_t*)arg;
    double lb = targs->lb;
    double hb = targs->hb;

    double res = 0;
    for(double x = lb; x < hb; x += dX) {
        res += FUNC(x);
    }
    targs->res = res * dX;

    pthread_exit(0);
}

void destructthreadset()
{
    free(threadset.cl.load);
    free(threadset.cts);
}
